import mongoengine, bson

from FullstackScraper.settings import MONGO_HOST, MONGO_PORT, MONGO_DB, SERVER_PUBLICY_AVAILABLE

from flask import Flask
from flask_restful import Resource, Api

from FullstackScraper import models

from bson import objectid

app = Flask(__name__)
api = Api(app)

db = mongoengine.connect(db=MONGO_DB, 
                        host=MONGO_HOST, 
                        port=MONGO_PORT)

class ArticlesRouting(Resource):
    """Class handler for article objects"""
    
    def put(self):
        pass
    
    @classmethod
    def get(cls, id):
        """handler function for GET request"""
        try:
            obj_id = objectid.ObjectId(oid=id)
        except:
            return {"error": "bad id format"}
        
        article = models.Articles.objects(id=obj_id).first()
        if article:
            data = {
                    'title': article.title,
                    'link': article.link,
                    'id_categories': [str(cat.id) for cat in article.categories]
                    }       
        else:
            data = None
    
        return {'article': data}

class CategoriesRouting(Resource):
    def put(self):
        pass
    @classmethod
    def get(cls, id):
        
        try:
            obj_id = objectid.ObjectId(oid=id)
        except:
            return {"error": "bad id format"}
    
        category = models.Categories.objects(id=obj_id).first()
        if category:
            data = {'name': categoty.name}
        else:
            data = None

        return {'category': data}

api.add_resource(ArticlesRouting, '/article/id/<string:id>')
api.add_resource(CategoriesRouting, '/category/id/<string:id>')
#api.add_resource(ArticlesRouting, '/')

if __name__ == '__main__':
    if SERVER_PUBLICY_AVAILABLE:
        app.run(debug=True, host='0.0.0.0')
    else:
        app.run(debug=True)
