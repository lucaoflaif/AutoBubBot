import requests

from .settings import API_URL, BOT_TOKEN, ADMIN_GROUP_ID

def make_url():
    endpoint = 'sendMessage'
    return '/'.join([API_URL, 'bot' + BOT_TOKEN, endpoint])

def make_request(**kwargs):
    url = make_url()

    params = {'chat_id': ADMIN_GROUP_ID}

    req = requests.get(url, params = {**params, **dict(**kwargs)})
    #print('REQ RESPONSE ', req.content)