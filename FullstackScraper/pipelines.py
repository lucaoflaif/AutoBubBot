# -*- coding: utf-8 -*-

from scrapy.exceptions import DropItem
from .models import Articles, Categories
import mongoengine as me

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

class CheckDuplicateArticlePipeline(object):
    def __init__(self, mongo_db, mongo_host, mongo_port):
        self.mongo_host = mongo_host
        self.mongo_port = mongo_port
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_host=crawler.settings.get('MONGO_HOST'),
            mongo_port=crawler.settings.get('MONGO_PORT'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'fullstackscraper_db')
        )
    
    def open_spider(self, spider):
        self.db = me.connect(db=self.mongo_db, host=self.mongo_host, port=self.mongo_port)
    
    def close_spider(self, spider):
        self.db.close()
    
    def process_item(self, item, spider):
        # if a same article is on db
        if Articles.objects(title=item['title'], link=item['link']).count():
            # drop the current item from crawler
            raise DropItem("'%s' -> already in DB" % item['title'])
        return item

class SaveArticlePipeline(object):
    collection_name = 'Articles'

    def __init__(self, mongo_db, mongo_host, mongo_port):
        self.mongo_host = mongo_host
        self.mongo_port = mongo_port
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_host=crawler.settings.get('MONGO_HOST'),
            mongo_port=crawler.settings.get('MONGO_PORT'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'fullstackscraper_db')
        )

    def open_spider(self, spider):
        self.db = me.connect(db=self.mongo_db, host=self.mongo_host, port=self.mongo_port)

    def close_spider(self, spider):
        self.db.close()

    def process_item(self, item, spider):
        Articles(title=item['title'],
                link=item['link'],
                categories=self.save_and_get_categories(item)) \
        .save()

        return item
    
    def save_and_get_categories(self, item):

        category_istances = []

        for category in item['categories']:
            category_queryset = Categories.objects(name=category)
            # if this isn't saved yet
            if not category_queryset:
                # save it
                category_istances.append(Categories(name=category).save())
            else:
                category_istances.append(category_queryset[0])

        return category_istances
