# -*- coding: utf-8 -*-

import bson
import json
import time

import mongoengine as me

from mongoengine import signals

from .items import Article

from . import bot_utils as bu

me.connect('fullstackscraper_db', host='localhost')

class Categories(me.Document):
    name = me.StringField(required=True)

class Articles(me.Document):
    title = me.StringField(required=True)
    link = me.URLField(required=True)
    categories = me.ListField(me.ReferenceField(Categories))

    @classmethod
    def post_save(cls, sender, document, **kwargs):
        #logging.debug("Post Save: %s" % document.name)
        if 'created' in kwargs:
            if kwargs['created']:
                #logging.debug("Created")
                #print('created ', document)

                text = ("<b>New save!</b>\n"
                        "<b>Id:</b> {0.id}\n"
                        "<b>Title</b>: {0.title}\n"
                        "<b>Link</b>: {0.link}\n"
                        ).format(document)

                reply_markup = json.dumps({'inline_keyboard' : 
                    [
                        [
                            #callback_data => [document id, channel id target]
                            {
                                'text': 'Send to Broadcast',
                                'callback_data': json.dumps([str(document.id),-1001065270271])
                            },
                            {
                                'text':  'Send to Daily AI',
                                'callback_data': json.dumps([str(document.id),-1001304543864])
                            }
                        ],
                    ]
                })

                bu.make_request(text = text,
                                parse_mode = 'HTML',
                                reply_markup=reply_markup)
                time.sleep(0.5)
            else:
                #logging.debug("Updated")
                pass

signals.post_save.connect(Articles.post_save, sender=Articles)