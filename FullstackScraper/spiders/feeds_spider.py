import scrapy

import fake_useragent as ua

from ..items import Article
from ..settings import SCRAPER_LINKS

ua = ua.UserAgent()

class QuotesSpider(scrapy.Spider):
    name = "feeds"

    def start_requests(self):
        urls = SCRAPER_LINKS
        for url in urls:
            yield scrapy.Request(url=url, 
                                callback=self.parse, 
                                headers={"User-Agent": ua.random})

    def parse(self, response):
        for item in response.xpath(r'//item'):
            title = item.xpath(r'./title/text()').extract_first()
            link = item.xpath(r'./link/text()').extract_first()
            categories = item.xpath(r'./category/text()').extract()

            yield Article(title=title,
                            link=link,
                            categories=categories)